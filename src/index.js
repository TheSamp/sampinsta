import React from 'react';
import ReactDOM from 'react-dom';
import {USERVARS} from './components/Common.js';
import App from './components/App';


ReactDOM.render(
    <App/>,
    document.querySelector(USERVARS.ELEMENT)
);