export const USERVARS = {
    ELEMENT: '#sampInsta',
    ACCESSTOKEN: (window.sampinstaVARS ? window.sampinstaVARS.ACCESSTOKEN : null),
    FETCHITEMS: (window.sampinstaVARS ? (window.sampinstaVARS.limit || 12) : 12)
};